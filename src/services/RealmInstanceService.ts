import RealmService from './realm';

class RealmInstanceService {
  private realmInstance: Realm;
  constructor() {}
  init = (isAdmin, isSynced): Promise<Realm> => {
    if (this.realmInstance) {
      throw 'RealmInstanceService already initialized.';
    }
    return RealmService.setupRealm({isAdmin, isSynced })
      .then((realm: Realm) => {
        this.realmInstance = realm;
        return realm;
      });
  };
  get realm() {
    if (this.realmInstance === undefined) {
      throw 'RealmInstanceService not initialized.';
    }
    return this.realmInstance;
  }
}

export default new RealmInstanceService();