import config from '../../config/realm/config';
import common from '../../config/realm/config.common';
import Realm from 'realm';
import User = Realm.Sync.User;
import migrateRealm from './migrateRealm';
import Configuration = Realm.Configuration;

const loginToRos = (credentials: { username?: string; password?: string; }): Promise<User> => {
  console.log('In loginToRos');
  return Realm.Sync.User.login(config.rosLoginUrl, credentials.username, credentials.password);
};

const openRealm = (configuration: Configuration): Promise<Realm> => {
  console.log('In openRealm');
  return Realm.open({
    ...configuration,
    path: common.realmName,
  });
};

const openSyncedRealm = (user: User, configuration: Configuration): Promise<Realm> => {
  console.log('In openRealm');
  return Realm.open({
    ...configuration,
    path: common.realmName,
    sync: {
      user,
      url: config.rosRealmUrl,
    },
  });
};

// Returns Promise that resolves to a Realm instance
const setupRealm = (options?: { isAdmin?: boolean; isSynced?: boolean, username?: string; password?: string; }): Promise<Realm> => {
  if (options.isSynced) {
    let credentials: any = {};
    if (options.isAdmin) {
      credentials.username = 'realm-admin';
      credentials.password = '';
    } else {
      credentials.username = options.username;
      credentials.password = options.password;
    }

    return loginToRos(credentials)
      .then((user: User) => {
        const syncConfiguration: Configuration = migrateRealm(true);
        // Finally after any migrations that took place we open the most recent version
        return openSyncedRealm(user, syncConfiguration);
      })
    ;
  }
  const configuration: Configuration = migrateRealm();
  return openRealm(configuration);
};

export default {
  migrateRealm,
  loginToRos,
  openRealm,
  setupRealm,
};