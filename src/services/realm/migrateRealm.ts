import { RealmOpenModels } from '../../resources/realm-models/';
import Realm from 'realm';
import migrations from '../../config/migrations/';
import common from '../../config/realm/config.common';
import Configuration = Realm.Configuration;

export default (isSynced: boolean = false) => {
  if (isSynced) {
    return migrateSyncedRealm();
  }
  return migrateRealm();
};

const migrateRealm = (): Configuration => {
  // Will be -1 if the Realm at the path has never been opened before.
  let schemaVersion = Realm.schemaVersion(common.realmName);
  schemaVersion = schemaVersion !== -1 ? schemaVersion : 0;
  console.log(`Last Realm Schema Version: ${schemaVersion}`);
  if (migrations.length === 0) {
    return {schema: RealmOpenModels, path: common.realmName };
  }
  // 2. Get the index of the migration where we are currently atrr
  let nextMigrationsIndex = -1;
  for (let index = 0; index < migrations.length; index++) {
    const migration = migrations[index];
    if (migration.schemaVersion === schemaVersion) {
      nextMigrationsIndex = index;
      break;
    }
  }
  // 3. Lets move onto the next on, since we know that this one has already been migrated to
  nextMigrationsIndex++;
  // 4. The next migration and all others that follow we are going to incrementally migrate the Realm
  for (; nextMigrationsIndex < migrations.length; nextMigrationsIndex++) {
    const migratedRealm: Realm = new Realm({
      ...migrations[nextMigrationsIndex],
      path: common.realmName,
    });
    migratedRealm.close();
  }

  // 5. Now that we have migrated the Realm up to the current version let's return the proper Configuration
  return {
    schema: RealmOpenModels,
    schemaVersion: migrations[migrations.length - 1].schemaVersion,
  };
};

// TODO: Placeholder for now until we fully understand how the ADDITIVE schema mode works for Synced Realms.
// https://stackoverflow.com/questions/48371500/can-you-perform-linear-migrations-on-a-synced-realm-in-realm-js
const migrateSyncedRealm = () => {
  if (migrations.length === 0) {
    return {schema: RealmOpenModels};
  }
  return {
    schema: RealmOpenModels,
    schemaVersion: migrations[migrations.length - 1].schemaVersion,
  };
};