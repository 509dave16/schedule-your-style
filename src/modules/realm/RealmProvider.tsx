import PropTypes from 'prop-types';
import { Component } from 'react';

export interface Props {
  realm: Realm;
}

export default class RealmProvider extends Component<Props> {
  static childContextTypes = {
    reactRealmInstance: PropTypes.object,
  };

  getChildContext() {
    return {
      reactRealmInstance: this.props.realm,
    };
  }

  render() {
    return this.props.children;
  }
}