import PropTypes from 'prop-types';
import React from 'react';
import hoistNonReactStatic from 'hoist-non-react-statics';

function connectRealm(WrappedComponent: React.ComponentClass, realmPropKey: string = 'realm') {
  class ConnectedRealmComponent extends React.Component {
    static contextTypes = {
      reactRealmInstance: PropTypes.object,
    };

    constructor(props, context) {
      super(props, context);
    }

    getProps = () => ({
        [realmPropKey]: this.context.reactRealmInstance,
        ...this.props,
    });

    render() {
      return <WrappedComponent {...this.getProps()} />;
    }
  }

  hoistNonReactStatic(ConnectedRealmComponent, WrappedComponent);

  return ConnectedRealmComponent;
}

export default connectRealm;