import { DrawerNavigator, StackNavigator } from 'react-navigation';
import Ionicons from 'react-native-vector-icons/Ionicons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';

import * as React from 'react';
import { createDefaultItemScreen, createDefaultListScreen, createDefaultListScreen2 } from '../../utils/ScreenUtil';
import { EventModel, EventModelStatic } from '../../resources/realm-models/EventModel';
import { EventTypeModel, EventTypeModelStatic } from '../../resources/realm-models/EventTypeModel';
import { OutfitModel, OutfitModelStatic } from '../../resources/realm-models/OutfitModel';

export default DrawerNavigator({
    Events: {
      screen: StackNavigator({
        Events: {
          screen: createDefaultListScreen<EventModel>(EventModelStatic),
          navigationOptions: {title: 'Events'},
        },
        Event: {
          screen: createDefaultItemScreen<EventModel>(EventModelStatic),
          navigationOptions: {title: 'Event'},
        },
      }, {initialRouteName: 'Events'}),
      navigationOptions: {
        drawerLabel: 'Events',
        drawerIcon: ({tintColor, focused}) => (
          <FontAwesome
            name={focused ? 'calendar' : 'calendar-o'}
            size={20}
            style={{color: tintColor}}
          />
        ),
      },
    },
    EventTypes: {
      screen: StackNavigator({
        EventTypes: {
          screen: createDefaultListScreen<EventTypeModel>(EventTypeModelStatic),
          navigationOptions: {title: 'Event Types'},
        },
        EventType: {
          screen: createDefaultItemScreen<EventTypeModel>(EventTypeModelStatic),
          navigationOptions: {title: 'Event Type'},
        },
      }, {initialRouteName: 'EventTypes'}),
      navigationOptions: {
        drawerLabel: 'Event Types',
        drawerIcon: ({tintColor, focused}) => (
          <FontAwesome
            name='tags'
            size={20}
            style={{color: tintColor}}
          />
        ),
      },
    },
    Outfits: {
      screen: StackNavigator({
        Outfits: {
          screen: createDefaultListScreen<OutfitModel>(OutfitModelStatic),
          navigationOptions: {title: 'Outfits'},
        },
        Outfit: {
          screen: createDefaultItemScreen<OutfitModel>(OutfitModelStatic),
          navigationOptions: {title: 'Outfit'},
        },
      }, {initialRouteName: 'Outfits'}),
      navigationOptions: {
        drawerLabel: 'Outfits',
        drawerIcon: ({tintColor, focused}) => (
          <Ionicons
            name={focused ? 'ios-shirt' : 'ios-shirt-outline'}
            size={20}
            style={{color: tintColor}}
          />
        ),
      },
    },
  }, {
    // TODO: These aren't a part of the interface for some reason
    drawerOpenRoute: 'DrawerOpen',
    drawerCloseRoute: 'DrawerClose',
    drawerToggleRoute: 'DrawerToggle',
    initialRouteName: 'Outfits',
    contentOptions: {
      activeTintColor: '#e91e63',
    },
  }
);
