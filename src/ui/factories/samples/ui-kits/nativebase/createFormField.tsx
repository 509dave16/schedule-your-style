import React from 'react';
import { Item, Label, Input } from 'native-base';
import { FormValidationMessage } from 'react-native-elements';

export default (fieldLabel: string, fieldValue: string, showError: boolean, errorMessage: string, onChangeCallback: Function) => {
  console.log('creating field');
  return (
    <Item key={`${fieldLabel}`} floatingLabel={true}>
      <Label key={`${fieldLabel}-Form-Label`}>{ fieldLabel }</Label>
      <Input key={`${fieldLabel}-Form-Input`} error={showError} value={fieldValue} onChangeText={onChangeCallback}/>
      { showError && <FormValidationMessage key={`${fieldLabel}-Form-Validation-Message`}>{ errorMessage }</FormValidationMessage>}
    </Item>
  );
};