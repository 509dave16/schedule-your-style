import React from 'react';
import { ObjectModel } from '../../../../../interfaces/RealmObjectModel';
import { List, ListItem, Button, Icon, Text } from 'native-base';
import { ListViewDataSource } from 'react-native';

export default function<ModelType extends ObjectModel>(dataSource: ListViewDataSource, callbacks: { pressRight: Function, pressLeft: Function }) {

  const renderRight = (data: ModelType, secId, rowId, rowMap) => (
    <Button full danger onPress={callbacks.pressRight(data, secId, rowId, rowMap)}>
      <Icon active name='trash' />
    </Button>
  );

  const renderLeft = (data: ModelType, secId, rowId, rowMap) => (
    <Button full onPress={callbacks.pressLeft(data, secId, rowId, rowMap)}>
      <Icon active name='create' />
    </Button>
  );

  const renderRow = (data: ModelType) => (
    <ListItem style={{ alignSelf: 'center'}}>
      <Text> {data.getDisplayName()} </Text>
    </ListItem>
  );

  return (
    <List
      removeClippedSubviews={true}
      dataSource={dataSource}
      renderRow={renderRow}
      renderLeftHiddenRow={renderLeft}
      renderRightHiddenRow={renderRight}
      leftOpenValue={75}
      rightOpenValue={-75}
    />

  );
}