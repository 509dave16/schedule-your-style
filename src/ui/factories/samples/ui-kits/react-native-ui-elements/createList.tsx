import { List, ListItem } from 'react-native-elements';
import React from 'react';
import { ObjectModel } from '../../../../../interfaces/RealmObjectModel';

export default function<ModelType extends ObjectModel>(items, navigateCallback) {
  return (<List>
    {
      items.map((item: ModelType) => (
        <ListItem
          title={item.getDisplayName()}
          key={item.uuid}
          onPress={() => navigateCallback(item)}
        />
      ))
    }
  </List>);
}
