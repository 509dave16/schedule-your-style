import React from 'react';
import { FormInput, FormLabel, FormValidationMessage } from 'react-native-elements';

export default (fieldLabel: string, fieldValue: string, showError: boolean, errorMessage: string, onChangeCallback: Function) => {
  return ([
      <FormLabel key={`${fieldLabel}-Form-Label`}>{ fieldLabel }</FormLabel>,
      <FormInput key={`${fieldLabel}-Form-Input`} value={fieldValue} onChangeText={onChangeCallback}/>,
      showError && <FormValidationMessage key={`${fieldLabel}-Form-Validation-Message`}>{ errorMessage }</FormValidationMessage>,
  ]);
};