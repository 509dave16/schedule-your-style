import React, { Component } from 'react';
import {
  View,
  SafeAreaView,
  StyleSheet,
  ListView, ListViewDataSource,
} from 'react-native';
import { ObjectStatic } from '../../interfaces/RealmObjectStatic';
import { Button } from 'react-native-elements';
import { NavigationScreenProp } from 'react-navigation';
// Dev stuff
import createReactNativeElementsList from './samples/ui-kits/react-native-ui-elements/createList';
import createNativeBaseList from './samples/ui-kits/nativebase/createList';
import { ObjectModel } from '../../interfaces/RealmObjectModel';
import { RealmView } from '../../interfaces/RealmView';
import { getViewName, realmChangeRefresh } from '../../utils/RealmUtil';
import { inject, observer } from 'mobx-react/native';

export interface ListScreenOptions extends ScreenOptions {}
export default function createListScreen<ModelType extends ObjectModel>(view: RealmView, modelStatic: ObjectStatic, options: ListScreenOptions) {
  const storeName = `${view.schema}Store`;
  interface Props {
    navigation: NavigationScreenProp<any, any>;
  }

  interface State {
    items: Realm.Results<ModelType>;
  }

  @inject(storeName)
  @observer
  class ListScreen extends Component<Props, State> {
    public listDataSource: ListViewDataSource;
    // Dev Item Actions
    addItem = () => {
      this.props[storeName].addItem(modelStatic.getDefault());
    };
    removeFirst = () => {
      const first = this.props[storeName].results[0];
      this.props[storeName].remove(first);
    };
    removeAll = () => {
      const all = this.props[storeName].results;
      this.props[storeName].remove(all);
    };

    constructor(props) {
      super(props);
      this.listDataSource = new ListView.DataSource({ rowHasChanged: (item1, item2) => item1.uuid !== item2.uuid});
    }

    render() {
      const {navigate} = this.props.navigation;
      const store = this.props[storeName];
      const routeKey: string = `From${view.schema}ListTo${view.schema}Detail`;
      const pressRight = (data: ModelType, secId, rowId, rowMap) => {
        return () => {
          const key: string = `${secId}${rowId}`;
          rowMap[key].props.closeRow();
          store.delete(data);
        };
      };
      const navigateCallback = (data: ModelType, showRemoveAction: boolean = true) => navigate({ key: routeKey, routeName: view.schema, params: { uuid: data.uuid, showRemoveAction } });
      const pressLeft = (data: ModelType, secId, rowId, rowMap) => {
        return realmChangeRefresh(() => {
          navigateCallback(data, false);
          rowMap[`${secId}${rowId}`].props.closeRow();
        }).bind(this);
      };
      // Dev
      if (options.dev !== undefined) {
        return (
          <SafeAreaView style={styles.container}>
            <View style={styles.listContainer}>
              {/*{ createReactNativeElementsList(this.state.items, navigateCallback)}*/}
              {/*{ createNativeBaseList(this.listDataSource.cloneWithRows(this.state.items), { pressLeft, pressRight }) }*/}
              { createNativeBaseList(this.listDataSource.cloneWithRows(this.props[storeName].results), { pressLeft, pressRight }) }
            </View>
            {options.showDevActions ?
              (<View style={styles.actionsContainer}>
                <Button style={styles.action} onPress={this.addItem} title='Add'/>
                <Button style={styles.action} onPress={this.removeFirst} title='Remove'/>
                <Button style={styles.action} onPress={this.removeAll} title={'Remove All'}/>
              </View>)
              :
              (<Button onPress={() => {
                  navigate({ key: routeKey, routeName: view.schema, params: {}});
              }} title={`Add ${view.schema}`}/>)
            }
          </SafeAreaView>
        );
      }
      return undefined;
    }
  }
  return ListScreen;
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  listContainer: {
    flex: 9,
  },
  actionsContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  action: {
    flex: 1,
  },
});