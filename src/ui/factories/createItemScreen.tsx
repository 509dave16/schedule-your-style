import React, { Component } from 'react';
import {
  SafeAreaView,
} from 'react-native';
import { ObjectStatic } from '../../interfaces/RealmObjectStatic';
import { Button, FormInput, FormLabel, FormValidationMessage } from 'react-native-elements';
import { NavigationScreenProp } from 'react-navigation';
import PropertiesTypes = Realm.PropertiesTypes;
import { ModelFormConfig, PropertyFieldConfig } from '../../interfaces/ModelFormConfig';
import { isBoolenProperty, isComplexProperty, isDateProperty, isNumberProperty, isStringProperty, realmChangeRefresh } from '../../utils/RealmUtil';
import createRNEFormField from './samples/ui-kits/react-native-ui-elements/createFormField';
import { Form } from 'native-base';
import createNBFormField from './samples/ui-kits/nativebase/createFormField';
import { ObjectModel } from '../../interfaces/RealmObjectModel';
import { inject, observer } from 'mobx-react/native';

export interface ItemScreenOptions extends ScreenOptions {
}

export function createItemScreen<ModelType extends ObjectModel>(modelStatic: ObjectStatic, options: ItemScreenOptions) {
  interface Props {
    navigation: NavigationScreenProp<any, any>;
  }

  interface State {
    item: ModelType;
    displayName: string;
    actionUnderway: boolean;
  }
  const storeName = `${modelStatic.schema.name}Store`;
  @inject(storeName)
  @observer
  class ItemScreen extends Component<Props, State> {
    public showRemoveAction: boolean;

    addItem = () => {
      this.props[storeName].save(modelStatic.getDefault(this.state.displayName));
    };

    updateItem = () => {
      this.props[storeName].save({uuid: this.state.item.uuid, [modelStatic.displayNameKey]: this.state.displayName});
    };

    constructor(props) {
      super(props);
      this.showRemoveAction = this.props.navigation.state.params.showRemoveAction;
    }

    componentWillMount() {
      const uuid: string = this.props.navigation.state.params.uuid;
      console.log(`uuid: ${uuid}`);
      if (uuid) {
        const item: ModelType = this.props[storeName].findByUuid(uuid);
        this.setState({item, displayName: item.getDisplayName(), actionUnderway: false});
      } else {
        this.setState({item: undefined, displayName: '', actionUnderway: false});
      }
    }

    upsertItem = () => {
      if (this.state.actionUnderway) return;
      this.setState({ actionUnderway: true });
      if (this.state.item) {
        this.updateItem();
      } else {
        this.addItem();
      }
      this.props.navigation.goBack();
    };
    removeItem = () => {
      if (this.state.actionUnderway) return;
      this.setState({ actionUnderway: true });
      this.props[storeName].removeItem(this.state.item);
      this.props.navigation.goBack();
    };
    bindDisplayName = (displayName: string) => {
      this.setState({displayName});
    };

    render() {
      return this.renderContent();
    }

    renderContent() {
      // Dev
      if (options.dev) {
        const action = this.state.item ? 'Update' : 'Add';
        return (
          <SafeAreaView style={{flex: 1}}>
            {/* Fields */}
            {/*{ createRNEFormField('Display Name', this.state.displayName, !this.state.displayName, 'Cannot be empty.', this.bindDisplayName)}*/}
            {/*<Form>*/}
              {/*{ createNBFormField('Display Name', this.state.displayName, !this.state.displayName, 'Cannot be empty', this.bindDisplayName) }*/}
            {/*</Form>*/}
            { this.buildForm() }
            {/* Actions */}
            <Button
              title={`${action} ${modelStatic.schema.name}`}
              onPress={this.upsertItem}
            />
            {this.showRemoveAction && this.state.item && !this.state.actionUnderway ? (<Button onPress={this.removeItem} title={`Remove ${modelStatic.schema.name}`}/>) : undefined}
          </SafeAreaView>
        );
      }
      // Prod
      return undefined;
    }

    buildForm() {
      const propertyTypes: PropertiesTypes = modelStatic.schema.properties;
      const modelFormConfig: ModelFormConfig = modelStatic.formConfig;
      const fieldComponents: any[] = [];
      for (const propertyKey of Object.keys(propertyTypes)) {
        const propertyDef: Realm.ObjectSchemaProperty | string = propertyTypes[propertyKey];
        if (typeof propertyDef === 'string') {
          throw { details: 'string Property Definition of ObjectSchema.properties not allowed. Only ObjectSchemaProperty.' };
        }
        const propertyConfig: PropertyFieldConfig = modelFormConfig[propertyKey];
        if (!isComplexProperty(propertyDef) && !propertyConfig) {
          console.log(propertyKey);
          if (isNumberProperty(propertyDef)) {
            // fieldComponents.push(undefined);
          } else if (isBoolenProperty(propertyDef)) {
            // fieldComponents.push(undefined);
          } else if (isDateProperty(propertyDef)) {
            // fieldComponents.push(undefined);
          } else if (isStringProperty(propertyDef)) {
            console.log('string type');
            const comp = createNBFormField(propertyKey, this.state.displayName, !this.state.displayName, 'Cannot be empty', this.bindDisplayName);
            fieldComponents.push(comp);
          } else {
            throw Error(`Simple Property type must be one of the following: string, number, boolean, date. Was given ${propertyDef.type}`);
          }
        }
      }
      return fieldComponents;
    }
  }

  return ItemScreen;
}