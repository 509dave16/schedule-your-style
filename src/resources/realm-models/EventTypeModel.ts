import { TaxonModelName } from '../../config/constants/RealmConstants';
import { RecurrenceModel } from './RecurrenceModel';
import uuidv4 from 'uuid/v4';
import { ObjectStatic } from '../../interfaces/RealmObjectStatic';
import { TaxonModel } from './TaxonModel';
import ObjectSchema = Realm.ObjectSchema;
import { getModelDefaultFormConfig } from '../../utils/FormUtil';
import { ObjectModel } from '../../interfaces/RealmObjectModel';
import { EventModelName, EventTypeModelName, RecurrenceModelName } from '../../config/constants/RealmConstants';

const schema: Realm.ObjectSchema = {
  name: EventTypeModelName,
  primaryKey: 'uuid',
  properties: {
    uuid: {type: 'string'},
    name: {type: 'string'},
    frequency: {type: RecurrenceModelName},
    notifications: {type: 'list', objectType: RecurrenceModelName},
    events: {type: 'linkingObjects', objectType: EventModelName, property: 'eventType'},
    taxons: {type: 'list', objectType: TaxonModelName},
    isDeletable: {type: 'bool', default: true},
    isUpdatable: {type: 'bool', default: true},
  },
};

export const EventTypeModelStatic: ObjectStatic = {
  schema,
  formConfig: Object.assign(getModelDefaultFormConfig(), {}),
  displayNameKey: 'name',
  getDefault(displayName: string = 'My Event Type') {
    return {
      uuid: uuidv4(),
      name: displayName,
    };
  },
};

export class EventTypeModel implements ObjectModel {
  public static schema: ObjectSchema = schema;

  public uuid: string;
  public name: string;
  public frequency: RecurrenceModel;
  public notifications: Realm.List<RecurrenceModel>;
  public taxons: Realm.List<TaxonModel>;
  public isDeletable: boolean;
  public isUpdatable: boolean;

  public getDisplayName(): string {
    return this.name;
  }
  public setDisplayName(displayName: string): void {
    this.name = displayName;
  }
}