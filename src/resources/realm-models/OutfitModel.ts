import { TaxonModelName } from '../../config/constants/RealmConstants';
import { TaxonModel } from './TaxonModel';
import { EventModel } from './EventModel';
import uuidv4 from 'uuid/v4';
import { ObjectStatic } from '../../interfaces/RealmObjectStatic';
import ObjectSchema = Realm.ObjectSchema;
import { getModelDefaultFormConfig } from '../../utils/FormUtil';
import { ObjectModel } from '../../interfaces/RealmObjectModel';
import { EventModelName, OutfitModelName } from '../../config/constants/RealmConstants';

const schema: Realm.ObjectSchema = {
  name: OutfitModelName,
  primaryKey: 'uuid',
  properties: {
    uuid: {type: 'string'},
    name: {type: 'string'},
    images: {type: 'string[]'},
    taxons: {type: 'list', objectType: TaxonModelName},
    events: {type: 'linkingObjects', objectType: EventModelName, property: 'outfit'},
    isDeletable: {type: 'bool', default: true},
    isUpdatable: {type: 'bool', default: true},
  },
};

export const OutfitModelStatic: ObjectStatic = {
  schema,
  formConfig: Object.assign(getModelDefaultFormConfig(), {}),
  displayNameKey: 'name',
  getDefault(displayName: string = 'My Outfit') {
    return {
      uuid: uuidv4(),
      name: displayName,
    };
  },
};

export class OutfitModel implements ObjectModel {
  public static schema: ObjectSchema = schema;

  public uuid: string;
  public name: string;
  public images: string[];
  public taxons: Realm.List<TaxonModel>;
  public events: Realm.Results<EventModel>;
  public isDeletable: boolean;
  public isUpdatable: boolean;

  public getDisplayName(): string {
    return this.name;
  }
  public setDisplayName(displayName: string): void {
    this.name = displayName;
  }
}