import { TaxonomyModelName } from '../../config/constants/RealmConstants';
import { TaxonomyModel } from './TaxonomyModel';
import { OutfitModel } from './OutfitModel';
import uuidv4 from 'uuid/v4';
import { ObjectStatic } from '../../interfaces/RealmObjectStatic';
import { EventTypeModel } from './EventTypeModel';
import ObjectSchema = Realm.ObjectSchema;
import { ObjectModel } from '../../interfaces/RealmObjectModel';
import { EventTypeModelName, OutfitModelName, TaxonModelName } from '../../config/constants/RealmConstants';
import { getModelDefaultFormConfig } from '../../utils/FormUtil';

const schema: Realm.ObjectSchema = {
  name: TaxonModelName,
  primaryKey: 'uuid',
  properties: {
    uuid: {type: 'string'},
    name: {type: 'string'},
    taxonomies: {type: 'linkingObjects', objectType: TaxonomyModelName, property: 'taxons'},
    outfits: {type: 'linkingObjects', objectType: OutfitModelName, property: 'taxons'},
    eventTypes: {type: 'linkingObjects', objectType: EventTypeModelName, property: 'taxons'},
    isDeletable: {type: 'bool', default: true},
    isUpdatable: {type: 'bool', default: true},
  },
};

export const TaxonModelStatic: ObjectStatic = {
  schema,
  displayNameKey: 'name',
  formConfig: Object.assign(getModelDefaultFormConfig(), {}),
  getDefault(displayName: string = 'My Taxon') {
    return {
      uuid: uuidv4(),
      name: displayName,
    };
  },
};

export class TaxonModel implements ObjectModel {
  public static schema: ObjectSchema = schema;

  public uuid: string;
  public name: string;
  public taxonomies: Realm.Results<TaxonomyModel>;
  public outfits: Realm.Results<OutfitModel>;
  public eventTypes: Realm.Results<EventTypeModel>;
  public isDeletable: boolean;
  public isUpdatable: boolean;

  public getDisplayName(): string {
    return this.name;
  }
  public setDisplayName(displayName: string): void {
    this.name = displayName;
  }
}