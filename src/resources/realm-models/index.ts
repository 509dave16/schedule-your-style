
// WARNING: DONT USE THIS FOR OPENING A REALM
export const RealmModels: Realm.ObjectClass[] = [
  EventModel,
  EventTypeModel,
  OutfitModel,
  TaxonomyModel,
  TaxonModel,
  RecurrenceModel,
];

// WARNING: THIS IS ONLY FOR OPENING A REALM
export const RealmOpenModels: Realm.ObjectClass[] = [
  EventModel,
  EventTypeModel,
  OutfitModel,
  TaxonomyModel,
  TaxonModel,
  RecurrenceModel,
];

import { EventModel } from './EventModel';
import { EventTypeModel } from './EventTypeModel';
import { OutfitModel } from './OutfitModel';
import { TaxonomyModel } from './TaxonomyModel';
import { TaxonModel } from './TaxonModel';
import { RecurrenceModel } from './RecurrenceModel';