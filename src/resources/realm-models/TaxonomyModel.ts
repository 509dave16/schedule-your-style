import { TaxonomyModelName } from '../../config/constants/RealmConstants';
import { TaxonModel } from './TaxonModel';
import uuidv4 from 'uuid/v4';
import { ObjectStatic } from '../../interfaces/RealmObjectStatic';
import ObjectSchema = Realm.ObjectSchema;
import { ObjectModel } from '../../interfaces/RealmObjectModel';
import { TaxonModelName } from '../../config/constants/RealmConstants';
import { getModelDefaultFormConfig } from '../../utils/FormUtil';

const schema: Realm.ObjectSchema = {
  name: TaxonomyModelName,
  primaryKey: 'uuid',
  properties: {
    uuid: {type: 'string'},
    name: {type: 'string'},
    taxons: {type: 'list', objectType: TaxonModelName},
    isDeletable: {type: 'bool', default: true},
    isUpdatable: {type: 'bool', default: true},
  },
};

export const TaxonomyModelStatic: ObjectStatic = {
  schema,
  displayNameKey: 'name',
  formConfig: Object.assign(getModelDefaultFormConfig(), {}),
  getDefault(displayName: string = 'My Event') {
    return {
      uuid: uuidv4(),
      name: displayName,
    };
  },
};

export class TaxonomyModel implements ObjectModel {
  public static schema: ObjectSchema = schema;

  public uuid: string;
  public name: string;
  public taxons: Realm.List<TaxonModel>;
  public isDeletable: boolean;
  public isUpdatable: boolean;

  public getDisplayName(): string {
    return this.name;
  }
  public setDisplayName(displayName: string): void {
    this.name = displayName;
  }
}