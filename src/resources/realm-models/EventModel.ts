import { RecurrenceModelName } from '../../config/constants/RealmConstants';
import { EventTypeModel } from './EventTypeModel';
import { OutfitModel } from './OutfitModel';
import uuidv4 from 'uuid/v4';
import { ObjectModel } from '../../interfaces/RealmObjectModel';
import { RecurrenceModel } from './RecurrenceModel';
import ObjectSchema = Realm.ObjectSchema;
import { getModelDefaultFormConfig } from '../../utils/FormUtil';
import { EventModelName, EventTypeModelName, OutfitModelName } from '../../config/constants/RealmConstants';
import { ObjectStatic } from '../../interfaces/RealmObjectStatic';

const schema: ObjectSchema = {
  name: EventModelName,
  primaryKey: 'uuid',
  properties: {
    uuid: {type: 'string'},
    name: {type: 'string'},
    eventType: {type: EventTypeModelName},
    notifications: {type: 'list', objectType: RecurrenceModelName},
    outfit: {type: OutfitModelName},
    date: {type: 'date'},
    isDeletable: {type: 'bool', default: true},
    isUpdatable: {type: 'bool', default: true},
  },
};

export const EventModelStatic: ObjectStatic = {
  schema,
  formConfig: Object.assign(getModelDefaultFormConfig(), {
    date: { hasField: false },
  }),
  getDefault(displayName: string = 'My Event') {
    return {
      uuid: uuidv4(),
      name: displayName,
      date: new Date(),
    };
  },
  displayNameKey: 'name',
};

export class EventModel implements ObjectModel {
  public static schema = schema;
  public uuid: string;
  public name: string;
  public eventType: EventTypeModel;
  public outfit: OutfitModel;
  public notifications: Realm.List<RecurrenceModel>;
  public date: Date;
  public isDeletable: boolean;
  public isUpdatable: boolean;

  public getDisplayName(): string {
    return this.name;
  }

  public setDisplayName(displayName: string): void {
    this.name = displayName;
  }
}