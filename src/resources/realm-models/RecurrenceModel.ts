/* tslint:disable: variable-name */

import { Recurrence } from 'later';
import { RecurrenceModelName } from '../../config/constants/RealmConstants';
import uuidv4 from 'uuid/v4';
import { ObjectStatic } from '../../interfaces/RealmObjectStatic';
import { EventTypeModel } from './EventTypeModel';
import { EventModel } from './EventModel';
import ObjectSchema = Realm.ObjectSchema;
import { ObjectModel } from '../../interfaces/RealmObjectModel';
import { EventModelName, EventTypeModelName } from '../../config/constants/RealmConstants';
import { getModelDefaultFormConfig } from '../../utils/FormUtil';

const schema: Realm.ObjectSchema = {
  name: RecurrenceModelName,
  primaryKey: 'uuid',
  properties: {
    uuid: {type: 'string'},
    name: {type: 'string'},
    isDeletable: {type: 'bool', default: true},
    isUpdatable: {type: 'bool', default: true},
    eventTypesByFrequency: {type: 'linkingObjects', objectType: EventTypeModelName, property: 'frequency'},
    eventTypesByNotifications: {type: 'linkingObjects', objectType: EventTypeModelName, property: 'notifications'},
    eventsByNotifications: {type: 'linkingObjects', objectType: EventModelName, property: 'notifications'},
    /** Time in seconds from midnight. */
    t: { type: 'int[]', optional: true },
    /** Seconds in minute. */
    s: { type: 'int[]', optional: true },
    /** Minutes in hour. */
    m: { type: 'int[]', optional: true },
    /** Hour in day. */
    h: { type: 'int[]', optional: true },
    /** Day of the month. */
    D: { type: 'int[]', optional: true },
    /** Day in week. */
    dw: { type: 'int[]', optional: true },
    /** Nth day of the week in month. */
    dc: { type: 'int[]', optional: true },
    /** Day in year. */
    dy: { type: 'int[]', optional: true },
    /** Week in month. */
    wm: { type: 'int[]', optional: true },
    /** ISO week in year. */
    wy: { type: 'int[]', optional: true },
    /** Month in year. */
    M: { type: 'int[]', optional: true },
    /** Year. */
    Y: { type: 'int[]', optional: true },
    /** After Modifiers */
    t_a: { type: 'int[]', optional: true },
    s_a: { type: 'int[]', optional: true },
    m_a: { type: 'int[]', optional: true },
    h_a: { type: 'int[]', optional: true },
    D_a: { type: 'int[]', optional: true },
    dw_a: { type: 'int[]', optional: true },
    dc_a: { type: 'int[]', optional: true },
    dy_a: { type: 'int[]', optional: true },
    wm_a: { type: 'int[]', optional: true },
    wy_a: { type: 'int[]', optional: true },
    M_a: { type: 'int[]', optional: true },
    Y_a: { type: 'int[]', optional: true },
    /** Before Modifiers */
    t_b: { type: 'int[]', optional: true },
    s_b: { type: 'int[]', optional: true },
    m_b: { type: 'int[]', optional: true },
    h_b: { type: 'int[]', optional: true },
    D_b: { type: 'int[]', optional: true },
    dw_b: { type: 'int[]', optional: true },
    dc_b: { type: 'int[]', optional: true },
    dy_b: { type: 'int[]', optional: true },
    wm_b: { type: 'int[]', optional: true },
    wy_b: { type: 'int[]', optional: true },
    M_b: { type: 'int[]', optional: true },
    Y_b: { type: 'int[]', optional: true },
  },
};

export const RecurrenceModelStatic: ObjectStatic = {
  schema,
  formConfig: Object.assign(getModelDefaultFormConfig(), {}),
  displayNameKey: 'name',
  getDefault(displayName: string = 'My Recurrence') {
    return {
      uuid: uuidv4(),
      name: displayName,
    };
  },
};

export class RecurrenceModel implements Recurrence, ObjectModel {
  public static schema: ObjectSchema = schema;

  // region Instance Members
  public eventTypesByFrequency: Realm.Results<EventTypeModel>;
  public eventTypesByNotifications: Realm.Results<EventTypeModel>;
  public eventsByNotifications: Realm.Results<EventModel>;
  public uuid: string;
  public name: string;
  public isDeletable: boolean;
  public isUpdatable: boolean;

  /** Time in seconds from midnight. */
  public t: number[];
  /** Seconds in minute. */
  public s: number[];
  /** Minutes in hour. */
  public m: number[];
  /** Hour in day. */
  public h: number[];
  /** Day of the month. */
  public D: number[];
  /** Day in week. */
  public dw: number[];
  /** Nth day of the week in month. */
  public dc: number[];
  /** Day in year. */
  public dy: number[];
  /** Week in month. */
  public wm: number[];
  /** ISO week in year. */
  public wy: number[];
  /** Month in year. */
  public M: number[];
  /** Year. */
  public Y: number[];
  /** After Modifiers */
  public t_a: number[];
  public s_a: number[];
  public m_a: number[];
  public h_a: number[];
  public D_a: number[];
  public dw_a: number[];
  public dc_a: number[];
  public dy_a: number[];
  public wm_a: number[];
  public wy_a: number[];
  public M_a: number[];
  public Y_a: number[];
  /** Before Modifiers */
  public t_b: number[];
  public s_b: number[];
  public m_b: number[];
  public h_b: number[];
  public D_b: number[];
  public dw_b: number[];
  public dc_b: number[];
  public dy_b: number[];
  public wm_b: number[];
  public wy_b: number[];
  public M_b: number[];
  public Y_b: number[];
  // endregion

  /*
   * Custom Time Periods and Modifiers
   * For acces to custom time periods created as extension to the later static type
   * and modifiers created on the later modifier static type.
   */
  [timeperiodAndModifierName: string]: number[] | undefined;

  public getDisplayName(): string {
    return this.name;
  }
  public setDisplayName(displayName: string): void {
    this.name = displayName;
  }
}