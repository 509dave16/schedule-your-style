import { RealmModels } from '../realm-models';
import { realmToMSTTypesMap } from '../../config/constants/MSTConstants';
import { types } from 'mobx-state-tree';
import { ModelType } from 'mobx-state-tree/dist/types/complex-types/model';
import { realmObjectSchemaIdentifierProperty } from '../../config/constants/MSTConstants';
const mstModels = {};
RealmModels.forEach((realmModel: Realm.ObjectClass) => {
  const schema: Realm.ObjectSchema = realmModel.schema;
  const identifierKey: string = realmModel.schema[realmObjectSchemaIdentifierProperty];
  const mstModelProps = {};
  for (const propKey in schema.properties) {
    if (!schema.properties.hasOwnProperty(propKey)) {
      continue;
    }
    const propDef: Realm.ObjectSchemaProperty = schema.properties[propKey] as Realm.ObjectSchemaProperty;
    const mstType = realmToMSTTypesMap[propDef.type];
    if (mstType === types.model) {
      mstModelProps[propKey] = types.maybe(types.late(() => mstModels[mstType]));
    } else if (mstType === types.array) {
      const mstObjectType = propDef.objectType;
      if (!mstObjectType) {
        mstModelProps[propKey] = mstType;
      } else {
        mstModelProps[propKey] = types.late(() => mstType(mstModels[mstObjectType]));
      }
    } else {
      mstModelProps[propKey] = mstType;
    }
  }
  mstModels[schema.name] = types.model(schema.name, mstModelProps);
});
export const MSTModels: { [modelKey: string]: ModelType<any, any>; } = mstModels;