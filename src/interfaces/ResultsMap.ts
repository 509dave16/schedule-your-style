export interface ResultsMap {
  [index: string]: Realm.Results<Realm.ObjectClass>;
}