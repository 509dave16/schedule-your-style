export interface ViewResultsCallback {
  (schemaResults: Realm.Results<Realm.ObjectClass>): Realm.Results<Realm.ObjectClass>;
}

export interface RealmView {
  schema: string;
  name?: string; // If not specified uses schema
  resultsCallback?: ViewResultsCallback; // If not specified just does default realm.objects(schemaName)
}