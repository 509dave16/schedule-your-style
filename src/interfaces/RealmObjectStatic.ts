import { ModelFormConfig } from './ModelFormConfig';

export interface ObjectStatic extends Realm.ObjectClass {
  formConfig: ModelFormConfig;
  displayNameKey: string;
  getDefault(displayName?: string): any;
}