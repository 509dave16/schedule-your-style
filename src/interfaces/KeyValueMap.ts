export interface IKeyValueMap<ValueType> {
  [indexKey: string]: ValueType;
}