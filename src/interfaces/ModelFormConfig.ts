export interface ModelFormConfig {
  [fieldKey: string]: PropertyFieldConfig<any>;
}

export interface PropertyFieldConfig<T> {
  name?: string;
  noField?: boolean;
  getChoices?: GetChoices<T>;
  isValid?: IsValid<T>;
}

export interface GetChoices<T> {
  (): T[];
}

export interface IsValid<T> {
  (value: T): string|true;
}