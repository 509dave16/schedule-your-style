export interface ObjectModel {
  uuid: string;
  isDeletable: boolean;
  isUpdatable: boolean;

  getDisplayName(): string;

  setDisplayName(displayName: string): void;
}