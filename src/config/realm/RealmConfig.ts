export interface RealmConfig {
  rosRealmUrl: string;
  rosLoginUrl: string;
}