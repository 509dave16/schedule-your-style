import common from './config.common';
import { IRealmConfig } from './RealmConfig';

export default (host): IRealmConfig => {
  const url: string = `${host}:${common.rosPort}`;
  return {
    rosRealmUrl: `realm://${url}/~/${common.realmName}`,
    rosLoginUrl: `http://${url}`,
  };
};
