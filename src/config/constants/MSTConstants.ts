import { types } from 'mobx-state-tree';
import { BasicListPropertyTypes, ModelNames } from './RealmConstants';

export const typeMap = {
  'bool': types.boolean,
  'string': types.string,
  'float': types.number,
  'int': types.number,
  'double': types.number,
  'date': types.Date,
  'list': types.array,
  'linkingObjects': types.array,
  'date[]': types.array(types.Date),
  'float[]': types.array(types.number),
  'int[]': types.array(types.number),
  'double[]': types.array(types.number),
  'bool[]': types.array(types.boolean),
  'string[]': types.array(types.string),
};

ModelNames.forEach((modelName) => {
  typeMap[modelName] = types.model;
});

export const realmObjectSchemaIdentifierProperty = 'primaryKey';
export const realmToMSTTypesMap = typeMap;