// Model Names
export const EventModelName: string = 'Event';
export const EventTypeModelName: string = 'EventType';
export const OutfitModelName: string = 'Outfit';
export const RecurrenceModelName: string = 'Recurrence';
export const TaxonModelName: string = 'Taxon';
export const TaxonomyModelName: string = 'Taxonomy';
export const ModelNames: string[] = [
  EventModelName,
  EventTypeModelName,
  OutfitModelName,
  RecurrenceModelName,
  TaxonModelName,
  TaxonomyModelName,
];
// Property Types
export const ComplexPropertyTypes: string[] = ['data', 'list', 'linkingObjects'];
export const BasicListPropertyTypes: string[] = ['date[]', 'float[]', 'int[]', 'double[]', 'bool[]', 'string[]'];