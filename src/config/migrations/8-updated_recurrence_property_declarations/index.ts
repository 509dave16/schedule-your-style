import migrationCallback from './migration';

const schemas = [];

import EventModel from './EventModel';
schemas.push(EventModel);

import EventTypeModel from './EventTypeModel';
schemas.push(EventTypeModel);

import OutfitModel from './OutfitModel';
schemas.push(OutfitModel);

import RecurrenceModel from './RecurrenceModel';
schemas.push(RecurrenceModel);

import TaxonModel from './TaxonModel';
schemas.push(TaxonModel);

import TaxonomyModel from './TaxonomyModel';
schemas.push(TaxonomyModel);


export default {
    schema: schemas,
    schemaVersion: 8,
    migration: migrationCallback,
};