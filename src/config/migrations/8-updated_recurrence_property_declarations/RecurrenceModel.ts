export default {
  "name": "Recurrence",
  "primaryKey": "uuid",
  "properties": {
    "uuid": { "type": "string" },
    "name": { "type": "string" },
    "isDeletable": { "type": "bool", "default": true },
    "isUpdatable": { "type": "bool", "default": true },
    "eventTypesByFrequency": {
      "type": "linkingObjects",
      "objectType": "EventType",
      "property": "frequency"
    },
    "eventTypesByNotifications": {
      "type": "linkingObjects",
      "objectType": "EventType",
      "property": "notifications"
    },
    "eventsByNotifications": {
      "type": "linkingObjects",
      "objectType": "Event",
      "property": "notifications"
    },
    "t": { "type": "int[]", "optional": true },
    "s": { "type": "int[]", "optional": true },
    "m": { "type": "int[]", "optional": true },
    "h": { "type": "int[]", "optional": true },
    "D": { "type": "int[]", "optional": true },
    "dw": { "type": "int[]", "optional": true },
    "dc": { "type": "int[]", "optional": true },
    "dy": { "type": "int[]", "optional": true },
    "wm": { "type": "int[]", "optional": true },
    "wy": { "type": "int[]", "optional": true },
    "M": { "type": "int[]", "optional": true },
    "Y": { "type": "int[]", "optional": true },
    "t_a": { "type": "int[]", "optional": true },
    "s_a": { "type": "int[]", "optional": true },
    "m_a": { "type": "int[]", "optional": true },
    "h_a": { "type": "int[]", "optional": true },
    "D_a": { "type": "int[]", "optional": true },
    "dw_a": { "type": "int[]", "optional": true },
    "dc_a": { "type": "int[]", "optional": true },
    "dy_a": { "type": "int[]", "optional": true },
    "wm_a": { "type": "int[]", "optional": true },
    "wy_a": { "type": "int[]", "optional": true },
    "M_a": { "type": "int[]", "optional": true },
    "Y_a": { "type": "int[]", "optional": true },
    "t_b": { "type": "int[]", "optional": true },
    "s_b": { "type": "int[]", "optional": true },
    "m_b": { "type": "int[]", "optional": true },
    "h_b": { "type": "int[]", "optional": true },
    "D_b": { "type": "int[]", "optional": true },
    "dw_b": { "type": "int[]", "optional": true },
    "dc_b": { "type": "int[]", "optional": true },
    "dy_b": { "type": "int[]", "optional": true },
    "wm_b": { "type": "int[]", "optional": true },
    "wy_b": { "type": "int[]", "optional": true },
    "M_b": { "type": "int[]", "optional": true },
    "Y_b": { "type": "int[]", "optional": true }
  }
}
;