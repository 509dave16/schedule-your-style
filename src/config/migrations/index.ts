const migrations = [];

import migration1 from './1-initial-sys-schema-definitions/';
migrations.push(migration1);

import migration2 from './2-added-updatable-and-deletable-to-object-schemas/';
migrations.push(migration2);

import migration3 from './3-added-event-type-to-recurrence/';
migrations.push(migration3);

import migration4 from './4-updated_eventtype_recurrence_relationships/';
migrations.push(migration4);

import migration5 from './5-updated_event_recurrence_relationships/';
migrations.push(migration5);

import migration6 from './6-updated_event_notifications_property/';
migrations.push(migration6);

import migration7 from './7-updated_eventtype_taxon_relationships/';
migrations.push(migration7);

import migration8 from './8-updated_recurrence_property_declarations/';
migrations.push(migration8);

export default migrations;