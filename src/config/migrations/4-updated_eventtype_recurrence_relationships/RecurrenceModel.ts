export default {
  'name': 'Recurrence',
  'primaryKey': 'uuid',
  'properties': {
    'uuid': {'type': 'string'},
    'name': {'type': 'string'},
    'isDeletable': {'type': 'bool', 'default': true},
    'isUpdatable': {'type': 'bool', 'default': true},
    'eventTypesByFrequency': {
      'type': 'linkingObjects',
      'objectType': 'EventType',
      'property': 'frequency'
    },
    'eventTypesByNotifications': {
      'type': 'linkingObjects',
      'objectType': 'EventType',
      'property': 'notifications'
    },
    't': 'int?[]',
    's': 'int?[]',
    'm': 'int?[]',
    'h': 'int?[]',
    'D': 'int?[]',
    'dw': 'int?[]',
    'dc': 'int?[]',
    'dy': 'int?[]',
    'wm': 'int?[]',
    'wy': 'int?[]',
    'M': 'int?[]',
    'Y': 'int?[]',
    't_a': 'int?[]',
    's_a': 'int?[]',
    'm_a': 'int?[]',
    'h_a': 'int?[]',
    'D_a': 'int?[]',
    'dw_a': 'int?[]',
    'dc_a': 'int?[]',
    'dy_a': 'int?[]',
    'wm_a': 'int?[]',
    'wy_a': 'int?[]',
    'M_a': 'int?[]',
    'Y_a': 'int?[]',
    't_b': 'int?[]',
    's_b': 'int?[]',
    'm_b': 'int?[]',
    'h_b': 'int?[]',
    'D_b': 'int?[]',
    'dw_b': 'int?[]',
    'dc_b': 'int?[]',
    'dy_b': 'int?[]',
    'wm_b': 'int?[]',
    'wy_b': 'int?[]',
    'M_b': 'int?[]',
    'Y_b': 'int?[]'
  }
}
;