export default {
  'name': 'EventType',
  'primaryKey': 'uuid',
  'properties': {
    'uuid': {'type': 'string'},
    'name': {'type': 'string'},
    'recurrence': {'type': 'Recurrence'},
    'events': {
      'type': 'linkingObjects',
      'objectType': 'Event',
      'property': 'eventType'
    },
    'isDeletable': {'type': 'bool', 'default': true},
    'isUpdatable': {'type': 'bool', 'default': true}
  }
}
;