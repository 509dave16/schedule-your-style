export default {
  'name': 'Event',
  'primaryKey': 'uuid',
  'properties': {
    'uuid': {'type': 'string'},
    'name': {'type': 'string'},
    'eventType': {'type': 'EventType'},
    'outfit': {'type': 'Outfit'},
    'date': {'type': 'date'}
  }
}
;