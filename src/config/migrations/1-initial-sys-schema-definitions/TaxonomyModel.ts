export default {
  'name': 'Taxonomy',
  'primaryKey': 'uuid',
  'properties': {
    'uuid': {'type': 'string'},
    'name': {'type': 'string'},
    'taxons': {'type': 'list', 'objectType': 'Taxon'}
  }
}
;