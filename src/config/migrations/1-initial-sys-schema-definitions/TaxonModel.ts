export default {
  'name': 'Taxon',
  'primaryKey': 'uuid',
  'properties': {
    'uuid': {'type': 'string'},
    'name': {'type': 'string'},
    'taxonomies': {
      'type': 'linkingObjects',
      'objectType': 'Taxonomy',
      'property': 'taxons'
    },
    'outfits': {
      'type': 'linkingObjects',
      'objectType': 'Outfit',
      'property': 'taxons'
    }
  }
}
;