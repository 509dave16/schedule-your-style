export default {
  'name': 'Outfit',
  'primaryKey': 'uuid',
  'properties': {
    'uuid': {'type': 'string'},
    'name': {'type': 'string'},
    'images': {'type': 'string[]'},
    'taxons': {'type': 'list', 'objectType': 'Taxon'},
    'events': {
      'type': 'linkingObjects',
      'objectType': 'Event',
      'property': 'outfit'
    }
  }
}
;