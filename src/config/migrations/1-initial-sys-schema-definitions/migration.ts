import Results = Realm.Results;

export default (oldRealm, newRealm) => {
  console.log(oldRealm.schemaVersion, newRealm.schemaVersion);
  if (oldRealm.schemaVersion < 1) {
    const oldObjects: Results<any> = oldRealm.objects('Event');
    const newObjects: Results<any> = newRealm.objects('Event');
    // loop through all objects and set the name property in the new schema
    for (let i = 0; i < oldObjects.length; i++) {
      delete(newObjects[i].images);
    }
  }
}