export default {
  'name': 'EventType',
  'primaryKey': 'uuid',
  'properties': {
    'uuid': {'type': 'string'},
    'name': {'type': 'string'},
    'recurrence': {'type': 'Recurrence'},
    'events': {
      'type': 'linkingObjects',
      'objectType': 'Event',
      'property': 'eventType'
    }
  }
}
;