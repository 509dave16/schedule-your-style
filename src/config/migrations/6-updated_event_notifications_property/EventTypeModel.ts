export default {
  'name': 'EventType',
  'primaryKey': 'uuid',
  'properties': {
    'uuid': {'type': 'string'},
    'name': {'type': 'string'},
    'frequency': {'type': 'Recurrence'},
    'notifications': {'type': 'list', 'objectType': 'Recurrence'},
    'events': {
      'type': 'linkingObjects',
      'objectType': 'Event',
      'property': 'eventType'
    },
    'isDeletable': {'type': 'bool', 'default': true},
    'isUpdatable': {'type': 'bool', 'default': true}
  }
}
;