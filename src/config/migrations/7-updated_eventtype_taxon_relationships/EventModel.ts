export default {
  'name': 'Event',
  'primaryKey': 'uuid',
  'properties': {
    'uuid': {'type': 'string'},
    'name': {'type': 'string'},
    'eventType': {'type': 'EventType'},
    'notifications': {'type': 'list', 'objectType': 'Recurrence'},
    'outfit': {'type': 'Outfit'},
    'date': {'type': 'date'},
    'isDeletable': {'type': 'bool', 'default': true},
    'isUpdatable': {'type': 'bool', 'default': true}
  }
}
;