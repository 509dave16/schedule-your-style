import React, { Component } from 'react';
import DrawerNavigator from './ui/navigators/DrawerNavigator';
import { Text, View } from 'react-native';
import RealmInstanceService from './services/RealmInstanceService';
import { Provider } from 'mobx-react/native';
import { createAppStores } from './utils/MobxUtil';

export interface Props {
}

export interface State {
  realm: Realm;
  stores: any;
  status: string;
}

export default class App extends Component<Props, State> {
  constructor(props) {
    super(props);
    this.state = {stores: undefined, realm: undefined, status: 'Loading App...'};
  }

  componentWillMount() {
    RealmInstanceService.init(true, false)
      .catch((e) => {
        const isTypeError = e instanceof TypeError;
        const isFailedNetworkRequest = e.message === 'Network request failed';
        if (isTypeError && isFailedNetworkRequest) {
          this.setState({status: 'Server could not be reached.'});
        } else if (isTypeError) {
          this.setState({status: e.message});
        } else {
          this.setState({status: 'Unknown error'});
        }
      })
      .then((realm: Realm) => {
        console.log('About to set Realm state');
        const stores = createAppStores();
        this.setState({realm, stores});
      })
    ;
  }

  render() {
    if (this.state.realm === undefined) {
      return (
        <View>
          <Text>{this.state.status}</Text>
        </View>
      );
    }
    return (
      <Provider  {...this.state.stores}>
          <DrawerNavigator/>
      </Provider>
    );
  }
}
