import { action, observable } from 'mobx';
import uuidv4 from 'uuid/v4';
import RealmInstanceService from '../services/RealmInstanceService';
import { RealmModels } from '../resources/realm-models';

export const createRealmResultsStore = (model: Realm.ObjectClass) => {
// export const createRealmResultsStore = (modelName: string) => {
  class RealmResultsStore {
    @observable results: Realm.Results<Realm.ObjectClass> = undefined;
    @action init = () => {
      this.results = RealmInstanceService.realm.objects(model.schema.name);
    };
    @action set = (results: Realm.Results<Realm.ObjectClass>) => {
      this.results = results;
    };
    @action save = (object: any) => {
      let update = false;
      if (object.uuid) {
        update = true;
      } else {
        object.uuid = uuidv4();
      }
      RealmInstanceService.realm.write(() => {
        RealmInstanceService.realm.create(model, object, update);
      });
      this.init();
    };
    @action delete = (object: Realm.Object) => {
      RealmInstanceService.realm.write(() => {
        RealmInstanceService.realm.delete(object);
      });
      this.init();
    };
    findByUuid = (uuid: string) => {
      return RealmInstanceService.realm.objects(model.schema.name).find((object: any) => object.uuid === uuid);
    }
  }
  return new RealmResultsStore();
};

export const createAppStores = () => {
  const stores = {};
  RealmModels.forEach((model) => {
    const store = createRealmResultsStore(model);
    store.init();
    stores[`${model.schema.name}Store`] = store;
  });
  return stores;
};