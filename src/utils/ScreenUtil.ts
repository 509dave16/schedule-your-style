import { ObjectStatic } from '../interfaces/RealmObjectStatic';
import createListScreen from '../ui/factories/createListScreen';
import { defaultViewResultsCallback } from './RealmUtil';
import { createItemScreen } from '../ui/factories/createItemScreen';
import { ObjectModel } from '../interfaces/RealmObjectModel';

export const createDefaultListScreen = <ModelType extends ObjectModel>(modelStatic: ObjectStatic) => {
  return createListScreen<ModelType>({
      schema: modelStatic.schema.name,
      resultsCallback: defaultViewResultsCallback,
    },
    modelStatic,
    { dev: true, showDevActions: false }
  );
};

export const createDefaultItemScreen = <ModelType extends ObjectModel>(modelStatic: ObjectStatic) => {
  return createItemScreen<ModelType>(modelStatic, { dev: true, showDevActions: false });
};