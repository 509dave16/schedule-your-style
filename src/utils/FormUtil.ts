import { ModelFormConfig } from '../interfaces/ModelFormConfig';

export const getModelDefaultFormConfig = (): ModelFormConfig => {
  return {
    uuid: { noField: true },
    isDeletable: { noField: true },
    isUpdatable: { noField: true },
  };
};