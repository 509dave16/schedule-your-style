import { types } from 'mobx-state-tree';
import { MSTModels } from '../resources/mst-models';
import RealmInstanceService from '../services/RealmInstanceService';

export const createListStoreForRealmModel = <RealmModelType>(schemaName, initialItems?: Array<RealmModelType>) => {
  let items: any = [];
  if (initialItems !== undefined) {
    items = initialItems;
  }
  return types.model({
    items: MSTModels[schemaName],
  })
  .actions(self => ({
    initialize: (newItems) => {
      self.items = newItems;
    },
    remove: (item) => {
      const realm: Realm = RealmInstanceService.realm;
      realm.write(() => {
        realm.delete(item);
      });
      self.items = realm.objects(schemaName);
    },
  }))
  .create({
    items: items,
  });
};