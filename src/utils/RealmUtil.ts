import { RealmView } from '../interfaces/RealmView';
import RealmQuery from 'realm-query';
import { ObjectModel } from '../interfaces/RealmObjectModel';
import { BasicListPropertyTypes, ComplexPropertyTypes, ModelNames } from '../config/constants/RealmConstants';

export const getViewName = (view: RealmView): string => {
  return view.name ? view.name : view.schema;
};

export const defaultViewResultsCallback = <ModelType extends ObjectModel>(schemaResults: Realm.Results<Realm.ObjectClass>) => {
  return RealmQuery.where(schemaResults).findAll<ModelType>();
};

export const isComplexProperty = (property: Realm.ObjectSchemaProperty): boolean => {
  const propertyType: string = property.type;
  return (
    ModelNames.find(name => name === propertyType) !== undefined ||
    ComplexPropertyTypes.find(thatPropertyType => thatPropertyType === propertyType) !== undefined ||
    isBasicListProperty(property)
  );
};

export const isBasicListProperty = (property: Realm.ObjectSchemaProperty): boolean => {
  return BasicListPropertyTypes.find(thatPropertyType => thatPropertyType === property.type) !== undefined;

};

export const isNumberProperty = (property: Realm.ObjectSchemaProperty): boolean => {
  const numberTypes: string[] = ['int', 'float', 'double'];
  return numberTypes.find(numberType => numberType === property.type) !== undefined;
};

export const isBoolenProperty = (property: Realm.ObjectSchemaProperty): boolean => {
  return property.type === 'bool';
};

export const isDateProperty = (property: Realm.ObjectSchemaProperty): boolean => {
  return property.type === 'date';
};

export const isStringProperty = (property: Realm.ObjectSchemaProperty): boolean => {
  return property.type === 'string';
};

export const realmChangeRefresh = function(callback: Function) {
  return function () {
    callback();
    this.forceUpdate();
  };
};